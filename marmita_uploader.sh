#!/usr/bin/env bash
set -u

# Print stuff
GREEN="\033[32;1m"
RED="\033[31;1m"
BOLD="\033[;1m"
NORMAL="\033[0m"

_BATCH_MODE='no'
_AUTO_GENERATE_THUMBNAIL='no'
_AUTO_UPLOAD_TO_ARCHIVE_ORG='no'
_AUTO_UPDATE_WEBSITE='no'
_AUTO_PUBLISH_WEBSITE='no'

#    ______                _   _
#   |  ____|              | | (_)
#   | |__ _   _ _ __   ___| |_ _  ___  _ __  ___
#   |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#   | |  | |_| | | | | (__| |_| | (_) | | | \__ \
#   |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#
#

function .usage() {
  echo "Usage: $0 [OPTIONS] EPISODE.ENV"
  echo ""
  echo "OPTIONS:"
  echo "  -b  Batch mode - no questions"
  echo "  -t  Auto generate thumbnail"
  echo "  -a  Auto upload to archive.org"
  echo "  -m  Auto write website markdown files"
  echo "  -p  Auto publish website (git push). Only works with -m"
  echo ""
  echo "note: options ordered by script sequece "
}

function .parse_input_flags() {
  while getopts ":btamp" opt; do
    case ${opt} in
    b)
      _BATCH_MODE='yes'
      shift $((OPTIND - 1))
      ;;
    t)
      _AUTO_GENERATE_THUMBNAIL='yes'
      shift $((OPTIND - 1))
      ;;
    a)
      _AUTO_UPLOAD_TO_ARCHIVE_ORG='yes'
      shift $((OPTIND - 1))
      ;;
    m)
      _AUTO_UPDATE_WEBSITE='yes'
      shift $((OPTIND - 1))
      ;;
    p)
      _AUTO_PUBLISH_WEBSITE='yes'
      shift $((OPTIND - 1))
      ;;
    \?)
      echo "invalid option ${opt}"
      .usage
      exit 1
      ;;
    esac
  done

  if [[ $# -ne 1 ]]; then
    echo "Invalid number of arguments $#"
    .usage
    exit 1
  fi
  EPISODE_ENV_FILE=$1
}

function .print_variables() {
  echo "_BATCH_MODE = $_BATCH_MODE"
  echo "_AUTO_GENERATE_THUMBNAIL = $_AUTO_GENERATE_THUMBNAIL"
  echo "_AUTO_UPLOAD_TO_ARCHIVE_ORG = $_AUTO_UPLOAD_TO_ARCHIVE_ORG"
  echo "_AUTO_UPDATE_WEBSITE = $_AUTO_UPDATE_WEBSITE"
  echo "_AUTO_PUBLISH_WEBSITE = $_AUTO_PUBLISH_WEBSITE"
}

function .load_variables() {
  # Load all variables
  EPISODE_ENV_FILE="$1"
  if [[ ! -f "${EPISODE_ENV_FILE}" ]]; then
    echo "'${EPISODE_ENV_FILE}' file does not exist. Copy from episode-s01e01.example.env"
    echo "Exiting..."
    exit 3
  fi
  source "${EPISODE_ENV_FILE}"
}

function .print_and_check_info() {
  echo -e "\n${BOLD}*************** Information *******************${NORMAL}"
  echo -e "Episode: Season ${GREEN}${SEASON_NUMBER}${NORMAL} Episode ${GREEN}${EPISODE_NUMBER}${NORMAL} (${GREEN}${_txxexx}${NORMAL})"
  echo -e "Audio File: ${GREEN}${AUDIO_PATH}${NORMAL}"
  echo -e "Title: ${GREEN}${TITLE}${NORMAL}"
  echo -e "Short Description: ${GREEN}${DESCRIPTION}${NORMAL}"
  echo -e ""

  if [[ "${_BATCH_MODE}" == 'no' ]]; then
    read -p "Continue? [y/N] " choice
    if [[ "$choice" =~ [yY] ]]; then
      echo ""
    else
      echo "Exiting"
      exit 0
    fi
  fi
}

function .check_pre_requisites() {
  # check commands
  if ! command -v "${IA_BIN}" >/dev/null 2>&1; then
    echo "ia (internet archive command line) is not present or executable."
    echo "Read README.md"
    exit 2
  fi

  if ! command -v "curl" >/dev/null 2>&1; then
    echo "curl is not present."
    echo "Read README.md"
    exit 2
  fi

  if ! command -v "jq" >/dev/null 2>&1; then
    echo "jq is not present."
    echo "Read README.md"
    exit 2
  fi

  if ! command -v "python3" >/dev/null 2>&1; then
    echo "python3 is not present."
    exit 2
  fi

  if ! command -v "display" >/dev/null 2>&1; then
    echo "display is not present."
    echo "find out how to install it <shrug>"
    exit 2
  fi

  # check paths and directories
  if [[ ! -f "${AUDIO_PATH}" ]]; then
    echo "AUDIO_PATH ${AUDIO_PATH} does not exist"
    exit 3
  fi
  if [[ ! -f "${BASE_IMAGE_PATH}" ]]; then
    echo "BASE_IMAGE_PATH ${BASE_IMAGE_PATH} does not exist"
    exit 3
  fi
  if [[ ! -f "${THUMBNAIL_PYTHON_SCRIPT}" ]]; then
    echo "THUMBNAIL_PYTHON_SCRIPT ${THUMBNAIL_PYTHON_SCRIPT} does not exist"
    exit 3
  fi
  if [[ ! -d "$(dirname "${WEBSITE_GIT_PATH}")" ]]; then
    echo "WEBSITE_GIT_PATH $(dirname "${WEBSITE_GIT_PATH}") directory does not exist"
    exit 3
  fi
  if [[ ! -d "$(dirname "${WEBSITE_GIT_PATH}${POST_RELATIVE_PATH}")" ]]; then
    echo "POST_RELATIVE_PATH $(dirname "${WEBSITE_GIT_PATH}${POST_RELATIVE_PATH}") directory does not exist"
    exit 3
  fi
  if [[ ! -d "$(dirname "${WEBSITE_GIT_PATH}${POST_IMAGE_RELATIVE_PATH}")" ]]; then
    echo "POST_IMAGE_RELATIVE_PATH $(dirname "${WEBSITE_GIT_PATH}${POST_IMAGE_RELATIVE_PATH}") directory does not exist"
    exit 3
  fi
  if [[ ! -d "$(dirname ${THUMBNAIL_PATH})" ]]; then
    echo "THUMBNAIL_PATH $(dirname ${THUMBNAIL_PATH}) directory does not exist"
    exit 3
  fi
}

function .generate_thumbnail() {
  echo -e "\n${BOLD}********** Generating Thumbnail (preview 5s) *************${NORMAL}"
  echo -e "Text: ${GREEN}${TITLE}${NORMAL}"
  echo -e ""

  if [[ "${_BATCH_MODE}" == 'no' && (-f "${THUMBNAIL_PATH}" || -f "${SMALL_THUMBNAIL_PATH}") ]]; then
    read -p "${THUMBNAIL_PATH} [OR] ${SMALL_THUMBNAIL_PATH} already exists. Override? [y/N] " choice
    if [[ "$choice" =~ [yY] ]]; then
      echo "Overriding file ${THUMBNAIL_PATH}/${SMALL_THUMBNAIL_PATH}"
    else
      return
    fi
  fi

  if [[ "${_BATCH_MODE}" == 'no' || ${_AUTO_GENERATE_THUMBNAIL} == 'yes' ]]; then
    python3 "${THUMBNAIL_PYTHON_SCRIPT}" "${BASE_IMAGE_PATH}" "${THUMBNAIL_PATH}" "${SMALL_THUMBNAIL_PATH}" "#${EPISODE_NUMBER} ${TITLE}"
  fi

  if [[ "${_BATCH_MODE}" == 'no' ]]; then
    display "${SMALL_THUMBNAIL_PATH}" &

    local _PID=$!
    sleep 5 && kill $_PID

    read -p "Thumbnails generated. Continue? [Y/n] " choice
    if [[ "$choice" =~ [nN] ]]; then
      echo "Stopping..."
      exit 0
    fi
  fi

}

function .upload_audio_to_archive_org() {
  echo -e "\n${BOLD}********** Publish to the Internet Archive (archive.org) *************${NORMAL}"
  echo -e "Identifier: ${GREEN}${ARCHIVE_ORG_IDENTIFIER}${NORMAL}"
  echo -e "Audio file: ${GREEN}${AUDIO_PATH}${NORMAL}"
  echo -e "Thumbnail file: ${GREEN}${THUMBNAIL_PATH}${NORMAL}"
  echo -e ""

  if [[ "${_BATCH_MODE}" == 'no' ]]; then
    read -p "Publish to archive.org? [Y/n] " choice
    if [[ "$choice" =~ [nN] ]]; then
      echo "Skip archive.org upload..."
      return
    fi
  fi

  if [[ "${_BATCH_MODE}" == 'no' || ${_AUTO_UPLOAD_TO_ARCHIVE_ORG} == 'yes' ]]; then
    echo "Uploading files to archive.org..."

    ${IA_BIN} ${IA_CONFIG} upload ${ARCHIVE_ORG_IDENTIFIER} \
      ${AUDIO_PATH} \
      ${THUMBNAIL_PATH} \
      ${SMALL_THUMBNAIL_PATH} \
      --metadata="mediatype:audio" \
      --metadata="language:por" \
      --metadata="date: ${PUBLISHED_AT}" \
      --retries 5

    if [[ $? -eq 0 ]]; then
      echo -e "OK - Episode published to ${GREEN}${ARCHIVE_ORG_DETAILS_URL}${NORMAL}"
    else
      echo "Upload failed? check login: ${IA_BIN} configure"
    fi
  fi
}

function .create_markdown_file() {
  local _markdown_post_file="${WEBSITE_GIT_PATH}${POST_RELATIVE_PATH}"
  local _image_file="${WEBSITE_GIT_PATH}${POST_IMAGE_RELATIVE_PATH}"

  echo -e "\n${BOLD}********** Create publishing Post (website/rss) *************${NORMAL}"
  echo -e "Destination Markdown File: ${GREEN}${_markdown_post_file}${NORMAL}"
  echo -e "Image File: ${GREEN}${_image_file}${NORMAL}"
  echo -e "Content: ${GREEN}\n${BODY_MD_TEXT}${NORMAL}"
  echo -e ""

  if [[ "${_BATCH_MODE}" == 'no' ]]; then
    if [[ -f "${_markdown_post_file}" ]]; then
      read -p "${_markdown_post_file} already exists. Override? [y/N] " choice
      if [[ "$choice" =~ [yY] ]]; then
        echo "Overriding file ${_markdown_post_file}"
      else
        echo "Skip post creation..."
        return
      fi
    else
      read -p "Create post (locally)? [Y/n] " choice
      if [[ "$choice" =~ [nN] ]]; then
        echo "Skip post creation..."
        return
      fi
    fi
  fi

  if [[ "${_BATCH_MODE}" == 'no' || ${_AUTO_UPDATE_WEBSITE} == 'yes' ]]; then
    echo "${MARKDOWN_TEMPLATE}" >"${_markdown_post_file}"
    cp "${THUMBNAIL_PATH}" "${_image_file}"
  fi
}

function .commit_and_push_website() {
  (
    echo -e "\n${BOLD}********** Publishing Post with git (website/rss) *************${NORMAL}"

    cd "${WEBSITE_GIT_PATH}"
    if [[ "${_BATCH_MODE}" == 'no' ]]; then
      while ! git diff --cached --exit-code &>/dev/null; do
        echo -e "${RED}There are staged changes not yet committed."
        echo -e "You need to fix this manually.${NORMAL}"
        echo -e "(you may open another shell and commit changes)"

        read -p "Try again? [Y/n] " choice
        if [[ "$choice" =~ [nN] ]]; then
          echo "Skip publishing website..."
          return
        fi
      done
    fi

    if [[ "${_BATCH_MODE}" == 'no' || ${_AUTO_PUBLISH_WEBSITE} == 'yes' ]]; then
      LOCAL_BRANCH=$(git name-rev --name-only HEAD)
      TRACKING_REMOTE=$(git config "branch.${LOCAL_BRANCH}.remote")
      REMOTE_URL=$(git config "remote.${TRACKING_REMOTE}.url")

      echo -e "Executing git pull..."
      git pull

      echo -e ""
      echo -e "File to commit: ${GREEN}$(basename ${POST_RELATIVE_PATH}) + $(basename ${POST_IMAGE_RELATIVE_PATH})${NORMAL}"
      echo -e "push to ${GREEN}${REMOTE_URL}${NORMAL}"
      echo -e "remote/branch: ${GREEN}${TRACKING_REMOTE}/${LOCAL_BRANCH}${NORMAL}"

      if [[ ${_AUTO_PUBLISH_WEBSITE} == 'no' ]]; then
        read -p "Commit and push? [Y/n] " choice
        if [[ "$choice" =~ [nN] ]]; then
          echo "Skip publishing website..."
          return
        fi
      fi

      git add "${POST_RELATIVE_PATH}"
      git add "${POST_IMAGE_RELATIVE_PATH}"
      git commit -m "Publishing $(basename "${POST_RELATIVE_PATH}") + $(basename "${POST_IMAGE_RELATIVE_PATH}")"
      git push

      [[ $? -ne 0 ]] && echo -e "${RED}Error pushing repository. Changes not pushed${NORMAL}"
    fi
  )

}

#    __  __       _
#   |  \/  |     (_)
#   | \  / | __ _ _ _ __
#   | |\/| |/ _` | | '_ \
#   | |  | | (_| | | | | |
#   |_|  |_|\__,_|_|_| |_|
#
#
function .main() {
  .parse_input_flags "$@"
  .load_variables "${EPISODE_ENV_FILE}"
  [[ "$_BATCH_MODE" == 'yes' ]] && .print_variables
  .check_pre_requisites
  .print_and_check_info

  .generate_thumbnail
  .upload_audio_to_archive_org

  .create_markdown_file
  .commit_and_push_website

}

.main "$@"
