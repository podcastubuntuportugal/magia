#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import datetime


def main(
    rss_feed="https://podcastubuntuportugal.org/feed/podcast/",
    template_file="env.template",
    output_dir="./env_files",
):
    template = ""
    with open(template_file) as f:
        template = f.read()

    r = requests.get(rss_feed)
    body = r.content

    xml_data = BeautifulSoup(body, "xml")
    episodes = xml_data.find_all("item")

    for i, episode in enumerate(episodes[::-1]):
        title = episode.title.string
        pub_date_original = episode.pubDate.string  # Thu, 14 Apr 2022 19:45:22 +0000
        guid = episode.guid.string  # https://podcastubuntuportugal.org/?p=2219
        audio_bytes = episode.enclosure["length"]  # eg: 56670711

        # https://media.blubrry.com/ubuntupt/podcastubuntuportugal.org/episodios/Podcast_Ubuntu_Portugal-Ep190.mp3
        audio_url = episode.enclosure["url"]

        description = episode.description.string

        # O Miguel está deprimido! Acabou de descobrir que daqui a uns anos – 5, talvez 10 – vai ter de gastar dinheiro num telefone novo. O Carrondo está todo contente […]
        summary = episode.description.string.splitlines()[0]

        duration = ""
        if episode.duration:
            duration = episode.duration.string  # eg: 58:43

        print(
            i,
            title,
            # "(No <content:encoded>)" if description == "" else "",
            "(No <itunes:duration>)" if duration == "" else "",
        )

        # Thu, 14 Apr 2022 19:45:22 +0000 -> 2022-02-14
        pub_date = datetime.datetime.strptime(
            pub_date_original, "%a, %d %b %Y %H:%M:%S %z"
        ).strftime("%Y-%m-%d")

        env_content = template
        env_content = env_content.replace("__EPISODE_NUMBER__", str(i))
        env_content = env_content.replace("__TITLE__", title)
        env_content = env_content.replace("__DESCRIPTION__", summary)
        env_content = env_content.replace("__AUDIO_URL__", audio_url)
        env_content = env_content.replace("__PUBLISHED_AT__", pub_date)
        env_content = env_content.replace("__AUDIO_DURATION__", duration)
        env_content = env_content.replace("__AUDIO_BYTES__", audio_bytes)
        env_content = env_content.replace(
            "__BODY_TEXT__", summary if description == "" else description
        )

        with open(f"{output_dir}/e{i:03}.env", "w") as f:
            f.write(env_content)


if __name__ == "__main__":
    main()
