# TODO

## Ubuntu 17.04 / Zesty Zapus / April 13, 2017

```
$ for i in {000..008}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e000 --> PUBLISHED_AT --> 2017-06-12
* e001 --> PUBLISHED_AT --> 2017-06-20
* e002 --> PUBLISHED_AT --> 2017-07-11
* e003 --> PUBLISHED_AT --> 2017-08-02
* e004 --> PUBLISHED_AT --> 2017-08-18
* e005 --> PUBLISHED_AT --> 2017-09-06
* e006 --> PUBLISHED_AT --> 2017-09-19
* e007 --> PUBLISHED_AT --> 2017-09-21
* e008 --> PUBLISHED_AT --> 2017-10-18

## Ubuntu 17.10 / Artful Aardvark / October 19, 2017

```
$ for i in {009..019}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e009 --> PUBLISHED_AT --> 2017-11-09
* e010 --> PUBLISHED_AT --> 2017-11-21
* e011 --> PUBLISHED_AT --> 2017-12-20
* e012 --> PUBLISHED_AT --> 2017-12-21
* e013 --> PUBLISHED_AT --> 2017-12-28
* e014 --> PUBLISHED_AT --> 2017-12-28
* e015 --> PUBLISHED_AT --> 2018-02-10
* e016 --> PUBLISHED_AT --> 2018-02-20
* e017 --> PUBLISHED_AT --> 2018-03-19
* e018 --> PUBLISHED_AT --> 2018-03-24
* e019 --> PUBLISHED_AT --> 2018-04-04

## Ubuntu 18.04 LTS / Bionic Beaver / April 26, 2018

```
$ for i in {020..029}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e020 --> PUBLISHED_AT --> 2018-04-26
* e021 --> PUBLISHED_AT --> 2018-05-10
* e022 --> PUBLISHED_AT --> 2018-06-11
* e023 --> PUBLISHED_AT --> 2018-06-19
* e024 --> PUBLISHED_AT --> 2018-07-25
* e025 --> PUBLISHED_AT --> 2018-08-27
* e026 --> PUBLISHED_AT --> 2018-09-14
* e027 --> PUBLISHED_AT --> 2018-09-23
* e028 --> PUBLISHED_AT --> 2018-09-28
* e029 --> PUBLISHED_AT --> 2018-10-14

## Ubuntu 18.10 / Cosmic Cuttlefish / October 18, 2018

```
$ for i in {030..053}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e030 --> PUBLISHED_AT --> 2018-10-20
* e031 --> PUBLISHED_AT --> 2018-10-28
* e032 --> PUBLISHED_AT --> 2018-11-01
* e033 --> PUBLISHED_AT --> 2018-11-04
* e034 --> PUBLISHED_AT --> 2018-11-09
* e035 --> PUBLISHED_AT --> 2018-11-14
* e036 --> PUBLISHED_AT --> 2018-11-18
* e037 --> PUBLISHED_AT --> 2018-11-28
* e038 --> PUBLISHED_AT --> 2018-11-29
* e039 --> PUBLISHED_AT --> 2018-12-06
* e040 --> PUBLISHED_AT --> 2018-12-13
* e041 --> PUBLISHED_AT --> 2018-12-20
* e042 --> PUBLISHED_AT --> 2018-12-28
* e043 --> PUBLISHED_AT --> 2019-01-03
* e044 --> PUBLISHED_AT --> 2019-01-10
* e045 --> PUBLISHED_AT --> 2019-01-19
* e046 --> PUBLISHED_AT --> 2019-02-01
* e047 --> PUBLISHED_AT --> 2019-02-07
* e048 --> PUBLISHED_AT --> 2019-02-14
* e049 --> PUBLISHED_AT --> 2019-02-24
* e050 --> PUBLISHED_AT --> 2019-03-12
* e051 --> PUBLISHED_AT --> 2019-03-19
* e052 --> PUBLISHED_AT --> 2019-04-03
* e053 --> PUBLISHED_AT --> 2019-04-07

## Ubuntu 19.04 / Disco Dingo / April 18, 2019

```
$ for i in {054..060}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e054 --> PUBLISHED_AT --> 2019-04-21
* e055 --> PUBLISHED_AT --> 2019-05-01
* e056 --> PUBLISHED_AT --> 2019-05-23
* e057 --> PUBLISHED_AT --> 2019-06-10
* e058 --> PUBLISHED_AT --> 2019-07-04
* e059 --> PUBLISHED_AT --> 2019-07-18
* e060 --> PUBLISHED_AT --> 2019-07-25

## Ubuntu 19.10 / Eoan Ermine / October 17, 2019

```
$ for i in {061..086}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e061 --> PUBLISHED_AT --> 2019-10-17
* e062 --> PUBLISHED_AT --> 2019-11-01
* e063 --> PUBLISHED_AT --> 2019-11-07
* e064 --> PUBLISHED_AT --> 2019-11-14
* e065 --> PUBLISHED_AT --> 2019-11-21
* e066 --> PUBLISHED_AT --> 2019-11-28
* e067 --> PUBLISHED_AT --> 2019-12-05
* e068 --> PUBLISHED_AT --> 2019-12-13
* e069 --> PUBLISHED_AT --> 2019-12-19
* e070 --> PUBLISHED_AT --> 2019-12-26
* e071 --> PUBLISHED_AT --> 2020-01-02
* e072 --> PUBLISHED_AT --> 2020-01-09
* e073 --> PUBLISHED_AT --> 2020-01-16
* e074 --> PUBLISHED_AT --> 2020-01-23
* e075 --> PUBLISHED_AT --> 2020-01-30
* e076 --> PUBLISHED_AT --> 2020-02-06
* e077 --> PUBLISHED_AT --> 2020-02-13
* e078 --> PUBLISHED_AT --> 2020-02-26
* e079 --> PUBLISHED_AT --> 2020-02-28
* e080 --> PUBLISHED_AT --> 2020-03-07
* e081 --> PUBLISHED_AT --> 2020-03-12
* e082 --> PUBLISHED_AT --> 2020-03-25
* e083 --> PUBLISHED_AT --> 2020-03-30
* e084 --> PUBLISHED_AT --> 2020-04-02
* e085 --> PUBLISHED_AT --> 2020-04-10
* e086 --> PUBLISHED_AT --> 2020-04-21

## Ubuntu 20.04 LTS / Focal Fossa / April 23, 2020

```
$ for i in {087..112}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e087 --> PUBLISHED_AT --> 2020-04-23
* e088 --> PUBLISHED_AT --> 2020-04-30
* e089 --> PUBLISHED_AT --> 2020-05-07
* e090 --> PUBLISHED_AT --> 2020-05-14
* e091 --> PUBLISHED_AT --> 2020-05-21
* e092 --> PUBLISHED_AT --> 2020-05-28
* e093 --> PUBLISHED_AT --> 2020-06-10
* e094 --> PUBLISHED_AT --> 2020-06-18
* e095 --> PUBLISHED_AT --> 2020-06-20
* e096 --> PUBLISHED_AT --> 2020-06-29
* e097 --> PUBLISHED_AT --> 2020-07-05
* e098 --> PUBLISHED_AT --> 2020-07-09
* e099 --> PUBLISHED_AT --> 2020-07-16
* e100 --> PUBLISHED_AT --> 2020-07-23
* e101 --> PUBLISHED_AT --> 2020-07-30
* e102 --> PUBLISHED_AT --> 2020-08-06
* e103 --> PUBLISHED_AT --> 2020-08-13
* e104 --> PUBLISHED_AT --> 2020-08-20
* e105 --> PUBLISHED_AT --> 2020-08-27
* e106 --> PUBLISHED_AT --> 2020-09-03
* e107 --> PUBLISHED_AT --> 2020-09-10
* e108 --> PUBLISHED_AT --> 2020-09-17
* e109 --> PUBLISHED_AT --> 2020-09-24
* e110 --> PUBLISHED_AT --> 2020-10-01
* e111 --> PUBLISHED_AT --> 2020-10-08
* e112 --> PUBLISHED_AT --> 2020-10-15

## Ubuntu 20.10 / Groovy Gorilla / October 22, 2020

```
$ for i in {113..138}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e113 --> PUBLISHED_AT --> 2020-10-22
* e114 --> PUBLISHED_AT --> 2020-10-29
* e115 --> PUBLISHED_AT --> 2020-11-05
* e116 --> PUBLISHED_AT --> 2020-11-12
* e117 --> PUBLISHED_AT --> 2020-11-19
* e118 --> PUBLISHED_AT --> 2020-11-26
* e119 --> PUBLISHED_AT --> 2020-12-03
* e120 --> PUBLISHED_AT --> 2020-12-10
* e121 --> PUBLISHED_AT --> 2020-12-17
* e122 --> PUBLISHED_AT --> 2020-12-24
* e123 --> PUBLISHED_AT --> 2020-12-31
* e124 --> PUBLISHED_AT --> 2021-01-07
* e125 --> PUBLISHED_AT --> 2021-01-14
* e126 --> PUBLISHED_AT --> 2021-01-21
* e127 --> PUBLISHED_AT --> 2021-01-28
* e128 --> PUBLISHED_AT --> 2021-02-04
* e129 --> PUBLISHED_AT --> 2021-02-11
* e130 --> PUBLISHED_AT --> 2021-02-18
* e131 --> PUBLISHED_AT --> 2021-02-25
* e132 --> PUBLISHED_AT --> 2021-03-04
* e133 --> PUBLISHED_AT --> 2021-03-11
* e134 --> PUBLISHED_AT --> 2021-03-18
* e135 --> PUBLISHED_AT --> 2021-03-25
* e136 --> PUBLISHED_AT --> 2021-04-01
* e137 --> PUBLISHED_AT --> 2021-04-08
* e138 --> PUBLISHED_AT --> 2021-04-15

## Ubuntu 21.04 / Hirsute Hippo / April 22, 2021

```
$ for i in {139..163}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e139 --> PUBLISHED_AT --> 2021-04-22
* e140 --> PUBLISHED_AT --> 2021-04-29
* e141 --> PUBLISHED_AT --> 2021-05-06
* e142 --> PUBLISHED_AT --> 2021-05-13
* e143 --> PUBLISHED_AT --> 2021-05-20
* e144 --> PUBLISHED_AT --> 2021-05-27
* e145 --> PUBLISHED_AT --> 2021-06-03
* e146 --> PUBLISHED_AT --> 2021-06-10
* e147 --> PUBLISHED_AT --> 2021-06-17
* e148 --> PUBLISHED_AT --> 2021-06-24
* e149 --> PUBLISHED_AT --> 2021-07-01
* e150 --> PUBLISHED_AT --> 2021-07-08
* e151 --> PUBLISHED_AT --> 2021-07-15
* e152 --> PUBLISHED_AT --> 2021-07-22
* e153 --> PUBLISHED_AT --> 2021-07-29
* e154 --> PUBLISHED_AT --> 2021-08-05
* e155 --> PUBLISHED_AT --> 2021-08-12
* e156 --> PUBLISHED_AT --> 2021-08-19
* e157 --> PUBLISHED_AT --> 2021-08-26
* e158 --> PUBLISHED_AT --> 2021-09-02
* e159 --> PUBLISHED_AT --> 2021-09-09
* e160 --> PUBLISHED_AT --> 2021-09-16
* e161 --> PUBLISHED_AT --> 2021-09-23
* e162 --> PUBLISHED_AT --> 2021-09-30
* e163 --> PUBLISHED_AT --> 2021-10-07

## Ubuntu 21.10 / Impish Indri / October 14, 2021

```
$ for i in {164..189}; do bash marmita_uploader.sh -btamp envs/e$i.env ; done
```

* e164 --> PUBLISHED_AT --> 2021-10-14
* e165 --> PUBLISHED_AT --> 2021-10-21
* e166 --> PUBLISHED_AT --> 2021-10-28
* e167 --> PUBLISHED_AT --> 2021-11-04
* e168 --> PUBLISHED_AT --> 2021-11-11
* e169 --> PUBLISHED_AT --> 2021-11-18
* e170 --> PUBLISHED_AT --> 2021-11-25
* e171 --> PUBLISHED_AT --> 2021-12-02
* e172 --> PUBLISHED_AT --> 2021-12-09
* e173 --> PUBLISHED_AT --> 2021-12-16
* e174 --> PUBLISHED_AT --> 2021-12-23
* e175 --> PUBLISHED_AT --> 2021-12-30
* e176 --> PUBLISHED_AT --> 2022-01-06
* e177 --> PUBLISHED_AT --> 2022-01-13
* e178 --> PUBLISHED_AT --> 2022-01-20
* e179 --> PUBLISHED_AT --> 2022-01-27
* e180 --> PUBLISHED_AT --> 2022-02-03
* e181 --> PUBLISHED_AT --> 2022-02-10
* e182 --> PUBLISHED_AT --> 2022-02-17
* e183 --> PUBLISHED_AT --> 2022-02-25
* e184 --> PUBLISHED_AT --> 2022-03-03
* e185 --> PUBLISHED_AT --> 2022-03-10
* e186 --> PUBLISHED_AT --> 2022-03-17
* e187 --> PUBLISHED_AT --> 2022-03-24
* e188 --> PUBLISHED_AT --> 2022-03-31
* e189 --> PUBLISHED_AT --> 2022-04-07
* e190 --> PUBLISHED_AT --> 2022-04-14
