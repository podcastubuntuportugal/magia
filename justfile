# Shows help
default:
    @just --list --justfile {{ justfile() }}

# upload everything
all episodio:
    ./marmita_uploader.sh -btamp envs/e{{ episodio }}.env

# just commit
commit episodio:
    ./marmita_uploader.sh -bmp envs/e{{ episodio }}.env

# upload video
video episodio:
    ./marmita_uploader.sh -by envs/e{{ episodio }}.env
