#!/usr/bin/env bash

#   __      __
#   \ \    / /
#    \ \  / /_ _ _ __ ___
#     \ \/ / _` | '__/ __|
#      \  / (_| | |  \__ \
#       \/ \__,_|_|  |___/
#
#
#### Different on every episode: ###################################################################

TITLE="Nas capas das Revistas"             # title without episode number.
EPISODE_NUMBER=27                         # number, not string
DESCRIPTION="Neste episódio convidámos o André Paula do LinuxTech PT, para vir falar-nos dos artigos que tem andado a escrever, também tivemos notícias e como sempre a agenda de eventos." # Short description
AUDIO_PATH="audios/e027.mp3"               # this file will be uploaded to archive.org
PUBLISHED_AT="2018-09-23"                  # publish date. eg: 2020-01-31
WEBSITE_FEATURED="false"                   # true | false # used in hugo template
WEBSITE_CATEGORIAS='["Episódio"]'           # website Categorias. Array like string. eg: '["cat1", "cat2"]'
TAGS="ubuntu,ubports,ota4,phone,kymera,slimbook,deepin,firefox,gnome,hollywood,robots,android,patch,mir,egmde,lubuntu,minimal"

# Markdown Body (for the website and podcast episode notes)
BODY_MD_TEXT="
Neste episódio convidámos o André Paula do LinuxTech PT, para vir falar-nos dos artigos que tem andado a escrever, também tivemos notícias e como sempre a agenda de eventos.
Os patronos podem ainda ter dose dupla do episódio, que vem recheada de saborosas notícias embebidas em molho de…

* https://slimbook.es/kymera-aqua-el-ordenador-gnu-linux-de-refrigeracion-liquida-custom
* https://slimbook.es/kymera-ventus-el-mejor-ordenador-de-sobremesa-para-gnu-linux
* https://ubports.com/blog/ubports-blog-1/post/ubuntu-touch-ota-4-release-166
* https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/62
* https://www.phoronix.com/scan.php?page=news_item&px=GNOME-3.30-Features
* https://www.linuxjournal.com/content/lights-camera-open-source-hollywood-turns-linux-new-code-sharing-initiative
* https://hackernoon.com/leaving-apple-google-e-first-beta-is-here-89e39f492c6f
* https://blog.ubuntu.com/2018/09/13/ubuntu-robot
* https://www.itwire.com/security/84300-google-will-not-patch-flaw-that-can-be-used-to-track-android-devices.html
* https://mir-server.io/
* https://www.phoronix.com/scan.php?page=news_item&px=Mir-Partial-NVIDIA-Support
* https://discourse.ubuntu.com/t/mir-news-17th-august-2018/7618
* https://www.phoronix.com/scan.php?page=news_item&px=Lubuntu-Openbox-Mi
* http://voices.canonical.com/tag/egmde/
* https://www.phoronix.com/scan.php?page=news_item&px=Mir-EGMDE-Experimental-X11
* https://snapcraft.io/egmde
* https://community.ubuntu.com/t/egmde-snap-update-0-2/7750r-Wayland
* https://twitter.com/WolframRoesler/status/1031979329591365633
"

#### Rarely changed: ###################################################################

AUTHOR="Podcast Ubuntu Portugal"              # We!!!
SEASON_NUMBER=1                               # number, not string
BASE_IMAGE_PATH="imgs/thumbnail-beaver.png"     # used on youtube and episode thumbnail. uploaded to archive.org. Square, between 1400-3000px
YOUTUBE_TITLE="E${EPISODE_NUMBER} - ${TITLE}" # youtube title
ARCHIVE_ORG_IDENTIFIER_PREFIX="pup-"          # archive.org unique identifier prefix

_txxexx=$(printf "e%02d" "${EPISODE_NUMBER}") # e01
_TxxExx=$(printf "E%02d" "${EPISODE_NUMBER}") # E01

SOCIAL_SHARE_POST_URL="https://podcastubuntuportugal.org/post/${_txxexx}" # Episode to share on social media (twitter)
SOCIAL_SHARE_MESSAGE="Novo Episódio: ${TITLE} ${SOCIAL_SHARE_POST_URL}"   # Message shared on social media

# Attribution and common text
FOOTER_MD_TEXT="
### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: \"Won't see it comin' (Feat Aequality & N'sorte d'autruche)\", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.
"

### Technical ################################################################################
IA_BIN="ia"                                   # internet archive api binary
IA_CONFIG="-c podcastubuntuportugal.ini"      # internet archive config file
GOOGLE_CREDENTIALS_PATH="client_secrets.json"
YOUTUBE_PRIVACY_STATUS="private"              # public|unlisted|private
YOUTUBE_LICENCE="creativeCommon"              # youtube|creativeCommon

VIDEO_PATH="videos/${_txxexx}.mp4"                                              # this file will be generated by ffmpeg
WEBSITE_GIT_PATH="../website/"                                                  # Hugo website GIT path
POST_RELATIVE_PATH="content/post/${_txxexx}.md"                                 # relative to $WEBSITE_GIT_PATH
ARCHIVE_ORG_IDENTIFIER="${ARCHIVE_ORG_IDENTIFIER_PREFIX}${_txxexx}"             # archive.org unique identifier prefix
ARCHIVE_ORG_DETAILS_URL="https://archive.org/details/${ARCHIVE_ORG_IDENTIFIER}" # link for uploaded content

THUMBNAIL_PATH="thumbnails/${_txxexx}.png"
SMALL_THUMBNAIL_PATH="thumbnails/${_txxexx}_480.png"
POST_IMAGE_RELATIVE_PATH="static/images/$(basename "${THUMBNAIL_PATH}")" # relative to $WEBSITE_GIT_PATH
THUMBNAIL_URL="images/$(basename "${THUMBNAIL_PATH}")"
THUMBNAIL_PYTHON_SCRIPT="create_thumbnail.py"
AUDIO_URL="https://media.blubrry.com/ubuntupt/archive.org/download/${ARCHIVE_ORG_IDENTIFIER}/$(basename "${AUDIO_PATH}")"
AUDIO_DURATION_SECONDS=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "${AUDIO_PATH}" | cut -d. -f1)

function .seconds_to_duration() {
  _duration_in_seconds=$1
  local _seconds=$(($_duration_in_seconds % 60))
  local _minutes=$(($_duration_in_seconds / 60 % 60))
  local _hours=$(($_duration_in_seconds / 3600))
  printf "%d:%02d:%02d" $_hours $_minutes $_seconds
}
AUDIO_DURATION=$(.seconds_to_duration "${AUDIO_DURATION_SECONDS}")
AUDIO_BYTES=$(stat -c%s ${AUDIO_PATH})

MARKDOWN_TEMPLATE="+++
title = \"E${EPISODE_NUMBER} ${TITLE}\"
itunes_title = \"${TITLE}\"
episode = ${EPISODE_NUMBER}
podcast_file = \"${AUDIO_URL}\"
podcast_duration = \"${AUDIO_DURATION}\"
podcast_bytes = \"${AUDIO_BYTES}\"
author = \"${AUTHOR}\"
date = \"${PUBLISHED_AT}\"
description = \"${DESCRIPTION}\"
thumbnail = \"${THUMBNAIL_URL}\"

featured = ${WEBSITE_FEATURED}
categories = ${WEBSITE_CATEGORIAS}
tags = [
$(IFS=,; for i in $TAGS ; do echo "  "\"$i\"\, ; done)
]
seasons = [\"$(printf "S%02d" "${SEASON_NUMBER}")\"]
aliases = [\"${_txxexx}\", \"${_TxxExx}\"]
+++
${BODY_MD_TEXT}
${FOOTER_MD_TEXT}"
