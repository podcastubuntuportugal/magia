#!/usr/bin/env bash

#   __      __
#   \ \    / /
#    \ \  / /_ _ _ __ ___
#     \ \/ / _` | '__/ __|
#      \  / (_| | |  \__ \
#       \/ \__,_|_|  |___/
#
#
#### Different on every episode: ###################################################################

TITLE="Descentralização!"             # title without episode number.
EPISODE_NUMBER=50                         # number, not string
DESCRIPTION="Neste episódio falamos sobre as habituais noticias sobre o Universo Ubuntu e temos como tema principal o software descentralizado e o fediverso, o que é, como utilizar e quais as suas vantagens. Já sabes, ouve, subscreve e partilha!" # Short description
AUDIO_PATH="audios/e050.mp3"               # this file will be uploaded to archive.org
PUBLISHED_AT="2019-03-12"                  # publish date. eg: 2020-01-31
WEBSITE_FEATURED="false"                   # true | false # used in hugo template
WEBSITE_CATEGORIAS='["Episódio"]'           # website Categorias. Array like string. eg: '["cat1", "cat2"]'
TAGS="NOT YET"

# Markdown Body (for the website and podcast episode notes)
BODY_MD_TEXT="
Neste episódio falamos sobre as habituais noticias sobre o Universo Ubuntu e temos como tema principal o software descentralizado e o fediverso, o que é, como utilizar e quais as suas vantagens. Já sabes, ouve, subscreve e partilha!

* https://wiki.ubuntu.com/XenialXerus/ReleaseNotes/ChangeSummary/16.04.6
* https://www.omgubuntu.co.uk/2019/03/ubuntu-19-04-mascot-disco-dingo-art
* https://www.omgubuntu.co.uk/2019/03/linux-kernel-5-0-released-this-is-whats-new
* https://linuxconfig.org/how-to-install-tweak-tool-on-ubuntu-18-04-bionic-beaver-linux
* https://ubports.com/blog/ubports-blog-1/post/call-for-testing-ubuntu-touch-ota-8-206
* https://snyk.io/blog/top-ten-most-popular-docker-images-each-contain-at-least-30-vulnerabilities/
* https://docs.ubuntu.com/security-certs/en/fips-faq
* https://joinmastodon.org/
* https://diasporafoundation.org/
* https://matrix.org/blog/home/
* https://nextcloud.com/
* https://joinpeertube.org/en/
* https://disroot.org/en
* https://framasoft.org/en/
* https://blog.ubuntu.com/2019/02/26/ubuntu-eal2-certified
* https://ubuntusecuritypodcast.org/
* https://capsule8.com/blog/millions-of-binaries-later-a-look-into-linux-hardening-in-the-wild/
* https://dot.kde.org/2019/02/20/kde-adding-matrix-its-im-framework
* https://matrix.org/blog/2019/02/20/welcome-to-matrix-kde/
* https://www.sylvia-ritter.com/
* https://static1.squarespace.com/static/55d13487e4b0c8582b04b231/55dd9b84e4b0e0734874f98e/5c6c3743eef1a1909cfad011/1550595934692/DiscoDingo_SylviaRitter.jpg
"

#### Rarely changed: ###################################################################

AUTHOR="Podcast Ubuntu Portugal"              # We!!!
SEASON_NUMBER=1                               # number, not string
BASE_IMAGE_PATH="imgs/thumbnail-cuttlefish.png"     # used on youtube and episode thumbnail. uploaded to archive.org. Square, between 1400-3000px
YOUTUBE_TITLE="E${EPISODE_NUMBER} - ${TITLE}" # youtube title
ARCHIVE_ORG_IDENTIFIER_PREFIX="pup-"          # archive.org unique identifier prefix

_txxexx=$(printf "e%02d" "${EPISODE_NUMBER}") # e01
_TxxExx=$(printf "E%02d" "${EPISODE_NUMBER}") # E01

SOCIAL_SHARE_POST_URL="https://podcastubuntuportugal.org/post/${_txxexx}" # Episode to share on social media (twitter)
SOCIAL_SHARE_MESSAGE="Novo Episódio: ${TITLE} ${SOCIAL_SHARE_POST_URL}"   # Message shared on social media

# Attribution and common text
FOOTER_MD_TEXT="
### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: \"Won't see it comin' (Feat Aequality & N'sorte d'autruche)\", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.
"

### Technical ################################################################################
IA_BIN="ia"                                   # internet archive api binary
IA_CONFIG="-c podcastubuntuportugal.ini"      # internet archive config file
GOOGLE_CREDENTIALS_PATH="client_secrets.json"
YOUTUBE_PRIVACY_STATUS="private"              # public|unlisted|private
YOUTUBE_LICENCE="creativeCommon"              # youtube|creativeCommon

VIDEO_PATH="videos/${_txxexx}.mp4"                                              # this file will be generated by ffmpeg
WEBSITE_GIT_PATH="../website/"                                                  # Hugo website GIT path
POST_RELATIVE_PATH="content/post/${_txxexx}.md"                                 # relative to $WEBSITE_GIT_PATH
ARCHIVE_ORG_IDENTIFIER="${ARCHIVE_ORG_IDENTIFIER_PREFIX}${_txxexx}"             # archive.org unique identifier prefix
ARCHIVE_ORG_DETAILS_URL="https://archive.org/details/${ARCHIVE_ORG_IDENTIFIER}" # link for uploaded content

THUMBNAIL_PATH="thumbnails/${_txxexx}.png"
SMALL_THUMBNAIL_PATH="thumbnails/${_txxexx}_480.png"
POST_IMAGE_RELATIVE_PATH="static/images/$(basename "${THUMBNAIL_PATH}")" # relative to $WEBSITE_GIT_PATH
THUMBNAIL_URL="images/$(basename "${THUMBNAIL_PATH}")"
THUMBNAIL_PYTHON_SCRIPT="create_thumbnail.py"
AUDIO_URL="https://media.blubrry.com/ubuntupt/archive.org/download/${ARCHIVE_ORG_IDENTIFIER}/$(basename "${AUDIO_PATH}")"
AUDIO_DURATION_SECONDS=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "${AUDIO_PATH}" | cut -d. -f1)

function .seconds_to_duration() {
  _duration_in_seconds=$1
  local _seconds=$(($_duration_in_seconds % 60))
  local _minutes=$(($_duration_in_seconds / 60 % 60))
  local _hours=$(($_duration_in_seconds / 3600))
  printf "%d:%02d:%02d" $_hours $_minutes $_seconds
}
AUDIO_DURATION=$(.seconds_to_duration "${AUDIO_DURATION_SECONDS}")
AUDIO_BYTES=$(stat -c%s ${AUDIO_PATH})

MARKDOWN_TEMPLATE="+++
title = \"E${EPISODE_NUMBER} ${TITLE}\"
itunes_title = \"${TITLE}\"
episode = ${EPISODE_NUMBER}
podcast_file = \"${AUDIO_URL}\"
podcast_duration = \"${AUDIO_DURATION}\"
podcast_bytes = \"${AUDIO_BYTES}\"
author = \"${AUTHOR}\"
date = \"${PUBLISHED_AT}\"
description = \"${DESCRIPTION}\"
thumbnail = \"${THUMBNAIL_URL}\"

featured = ${WEBSITE_FEATURED}
categories = ${WEBSITE_CATEGORIAS}
tags = [
$(IFS=,; for i in $TAGS ; do echo "  "\"$i\"\, ; done)
]
seasons = [\"$(printf "S%02d" "${SEASON_NUMBER}")\"]
aliases = [\"${_txxexx}\", \"${_TxxExx}\"]
+++
${BODY_MD_TEXT}
${FOOTER_MD_TEXT}"
